﻿using System.Web.Mvc;
using DemoContactApp.Models;

namespace DemoContactApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Contact");
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View(new ContactViewModel());
        }

        [HttpPost]
        public ActionResult Contact(ContactViewModel contact)
        {
            if (ModelState.IsValid)
            {
                return View("ThankYou",contact);
            }
            else
            {
                return RedirectToAction("Contact");
            }
        }
    }
}