# README #

This Project is a basic MVC project to demo a contact form with the following fields:

1. First name
2. Last name
3. Email address
4. Phone number
5. Contact message

## Additional features:

1. Upon submission of the contact form, backend will validation on the email format using MS [EmailAddressAttribute](https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.emailaddressattribute?redirectedfrom=MSDN&view=netframework-4.7.2)
2. If the form is valid, user will be redirect to a dedicated thank you page.
3. The thank you page include a re-gurgitation of the form submission details from the user.
4. The solution is secure from cross-site scripting using [AntiForgeryToken](https://docs.microsoft.com/en-us/dotnet/api/system.web.mvc.htmlhelper.antiforgerytoken?view=aspnet-mvc-5.2#System_Web_Mvc_HtmlHelper_AntiForgeryToken)

## How do I get set up?

* You'll need VS2019 and .Net Framework 4.7.2 installed.
* From VS2019 Start Window select "Clone a repository" option to begin.
* Follow the wizard to clone this repo onto your machine.
* After cloning complete, open the solution (DemoContactApp.sln).
* Once solution and project loaded, press F5 key to run and start debugging.
* Along with the source code, I included a "IISProfile.pubxml" for use to deploy this app to a local IIS server. 
* Right click on the project and select "Publish" to bring up that profile. 
* Reconfigure server address, user name, password ...etc. to your need before publish. *Please note this publish method is for demo only, DO NOT USE in production, a CICD pipeline is strongly recomended for production.

### Contribution guidelines

* This is one time demo project. It wont be maintain or accept any contribution. 